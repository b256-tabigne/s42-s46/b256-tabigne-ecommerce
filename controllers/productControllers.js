const Product = require("../models/Product.js");


module.exports.addProduct = (product) => {

	const { name, description, price, quantity } = product
	let newProduct = new Product({
		name,
		description,
		price,
		quantity
	});

	return newProduct.save().then((result, err) => {

		if(err) {

			return false;

		} else {

			return true;

		};
	});
};

module.exports.getAllProducts = () => {

	return Product.find({}).then(result => {

		return result;

	});
};


module.exports.getAllActiveProducts = () => {

	return Product.find({status: true}).then(result => {

		return result;

	})
}

module.exports.getProduct = ({ productId }) => {
  return Product.findById(productId).then((result, err) => {
    if (err) {
      return false;
    } else {
      return result;
    }
  });
};


module.exports.updatedProduct = (product, paramsId) => {

	let updatedProduct = {
		name : product.name,
		description : product.description,
		price : product.price
	}

	return Product.findByIdAndUpdate(paramsId.productId, updatedProduct).then((result, err) => {

		if(err) {

			return false;

		} else {

			return true;

		}
	})
}


module.exports.obtainProduct = (product, paramsId) => {

	let obtainProduct = {
		isActive : true
	}

	return Product.findByIdAndUpdate(paramsId.productId, obtainProduct).then((result, err) => {

		if(err) {

			return false;

		} else {

			return true;

		}
	})
}
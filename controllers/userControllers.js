const User = require("../models/User.js");
const Product = require("../models/Product.js");
const Order = require("../models/Order.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");

module.exports.registerUser = async (requestBody) => {

	const{ email, password, firstName, lastName } = requestBody

	let newUser = new User({
		email,
		firstName,
		lastName,
		password: bcrypt.hashSync(password, 10)

	})
	return newUser.save().then((user, err) => {

		if(err) {

			return false;

		} else {

			return true;

		}
	})
} 

module.exports.authenticateUser = (requestBody) => {

	return User.findOne({email: requestBody.email}).then(result => {

		if(result == null) {

			return false;

		} 
		else {

			const isPasswordCorrect = bcrypt.compareSync(requestBody.password, result.password)
			if(isPasswordCorrect) {

				return {access: auth.createAccessToken(result)}

			} 
			else {

				return false;

			};
		};
	});
};

module.exports.getProfile = (data) => {

	return User.findById(data.userId).then(result => {

		result.password = "";

		return result;

	});

};

module.exports.getUser = (userId) => {

	return User.findById(userId).then(result => {

		return result;

	})
}

module.exports.editBalance = (userId, updatedUser) => {

		return User.findByIdAndUpdate(userId, updatedUser).then((result, err) => {
			if(err) {
				return false;
			} 
			else {
				return true;
			}
		})
	}

module.exports.logoutUser = (res) => {

  res.status(200).send({ auth: false, token: null });

}


module.exports.editProduct = async (data) => {

	let productPrice = Product.findById(data.productId).then(product => {

		return product.price;
	})

	let isUserUpdated = await User.findById(data.userId).then(user => {

		user.updated.push({productId: data.productId, price: productPrice});

		return user.save().then((updated, err) => {

			if(err) {

				return false;

			} else {

				return true

			}
		})
	});

	let isProductUpdated = await Product.findById(data.productId).then(product => {

		product.updated.push({userId: data.userId});

		return product.save().then((updated, err) => {

			if(err) {

				return false;

			} else {

				return true;

			}
		})
	});

	if(isUserUpdated && isProductUpdated) {

		return true;

	} else {

		return false;

	}
}

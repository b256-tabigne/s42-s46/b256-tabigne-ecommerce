const Order = require("../models/Order.js");
const Product = require("../models/Product.js");
const User = require("../models/User.js");

module.exports.myPurchase = () => {

	return Order.find({}).then(result => {

		return result;

	});
};

module.exports.checkOut = (item) => {

	let newOrder = new Order({
		productId: item.productId,
		userId: item.userId
	});

	return newOrder.save().then((result, err) => {

		if(err) {

			return false;

		} else {

			return true;

		};
	});
};
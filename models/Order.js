const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
	
	productId: {
		type: Number,
		required: [true, "Must provide an existing productId"]
	},
	userId: {
		type: Number,
		required: [true, "Must provide an existing userId"]
	}

})
	

module.exports = mongoose.model("Order", orderSchema);
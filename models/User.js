const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({

	balance: {
		type: Number,
		default: 0
	},
	email: {
		type: String,
		required : [true, "Email is required"]
	},
	firstName: {
		type: String,
		required : [true, "firstName is required"]
	},
	lastName: {
		type: String,
		required : [true, "lastName is required"]
	},
	password: {
		type: String,
		required : [true, "password is required"]
	},
	role: {
		type: String,
		default : "user"
	}

})

module.exports = mongoose.model("User", userSchema);
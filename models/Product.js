const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	description: {
		type: String,
		required : [true, "description is required"]
	},
	name: {
		type: String,
		required : [true, "name is required"]
	},
	price: {
		type: Number,
		required : [true, "price is required"]
	},
	quantity: {
		type: Number,
		default: 1
	},
	status: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default : new Date()
	},
	updatedOn: {
		type: Date,
		default : new Date()
	}

})

module.exports = mongoose.model("Product", productSchema);
const express = require("express");
const router = express.Router();
const userController = require("../controllers/userControllers.js");
const auth = require("../auth.js");

router.post("/register", (req, res) => {

	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));

})

router.post("/login", (req, res) => {

	userController.authenticateUser(req.body).then(resultFromController => res.send(resultFromController));

})

router.get("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	userController.getProfile({userId: userData.id}).then(resultFromController => res.send(resultFromController));

})

router.get("/:userId", (req, res) => {

	userController.getUser(req.params.userId).then(resultFromController => res.send(resultFromController));

})


router.put("/balance/:userId", (req, res) => {

	const data ={
		user: req.body,
	}

	userController.editBalance(req.params.userId, data.user).then(resultFromController => res.send(resultFromController));

})



router.post("/editProduct", auth.verify, (req, res) => {

	const data ={
		// userId will be received from the request header
		userId: auth.decode(req.headers.authorization).id,
		productId: req.body.productId
	}

	userController.editProduct(data).then(resultFromController => res.send(resultFromController));
})





router.post("/logout", (req, res) => {

	userController.logoutUser(res).then(resultFromController => res.send(resultFromController));

})

module.exports = router;
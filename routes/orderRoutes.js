const express = require("express");
const router = express.Router();
const orderControllers = require("../controllers/orderControllers.js");
const auth = require("../auth.js")

router.get("/orders", auth.verify, (req, res) => {

	orderControllers.myPurchase().then(resultFromController => res.send(resultFromController));

})

router.post("/checkout", (req, res) => {

	const data = {
		order: req.body
	}

	orderControllers.checkOut(data.order).then(resultFromController => res.send(resultFromController));
})

module.exports = router;
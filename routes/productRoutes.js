const express = require("express");
const router = express.Router();
const productController = require("../controllers/productControllers.js");
const auth = require("../auth.js")

router.post("/createProduct", auth.verify, (req, res) => {

	const data = {
		product: req.body,
		role: auth.decode(req.headers.authorization)
	}
	console.log(data)
	if(data.role) {

		productController.addProduct(data.product).then(resultFromController => res.send(resultFromController));

	} else {

		res.send(false);

	}
});

router.get("/allProducts", (req, res) => {

	productController.getAllProducts().then(resultFromController => res.send(resultFromController));

})


router.get("/allActiveProducts", (req, res) => {

	productController.getAllActiveProducts().then(resultFromController => res.send(resultFromController));

})

router.get("/:productId", (req, res) => {

	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));

})


router.put("/update/:productId", auth.verify, (req, res) => {
  const productId = req.params.productId;

  const data = {
    product: req.body,
    status: auth.decode(req.headers.authorization).status,
    productId: productId
  };

  if (data.status) {
    productController.updatedProduct(data.product, data.productId)
      .then(resultFromController => res.send(resultFromController))
      .catch(error => {
        console.error("Error updating product:", error);
        res.send(false);
      });
  } else {
    res.send(false);
  }
});

module.exports = router;